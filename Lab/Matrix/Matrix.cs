﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Lab
{
    public class Matrix
    {
        private int nRows;
        private int nColumns;
        private List<List<double>> matrix;


        public Matrix(int nRows, int nColumns)
        {
            this.nRows = nRows;
            this.nColumns = nColumns;
            this.matrix = new List<List<double>>(nRows);

            for (int i = 0; i < nRows; i++)
            {
                List<double> row = new List<double>();
                for (int j = 0; j < nColumns; j++)
                {
                    row.Add(-1);
                }
                this.matrix.Add(row);
            }
        }

        public Matrix(string fileName)
        {
            matrix = new List<List<double>>();

            string[] lines = System.IO.File.ReadAllLines(fileName);

            foreach (string line in lines)
            {
                List<double> row = new List<double>();
                string[] values = line.Split(' ');

                foreach (string value in values)
                {
                    if (value.Length > 0)
                    {
                        row.Add(Double.Parse(value, CultureInfo.InvariantCulture));
                    }
                }

                matrix.Add(row);
            }

            nRows = matrix.Count;
            nColumns = matrix[0].Count;
        }

        private List<List<double>> getMatrix()
        {
            return this.matrix;
        } 

        public Matrix(Matrix mat)
        {
            this.matrix = new List<List<double>>();
            foreach (List<double> row in mat.getMatrix())
            {
                List<double> newRow = new List<double>();
                foreach (double d in row)
                {
                    newRow.Add(d);
                }

                this.matrix.Add(newRow);
            }

            nRows = matrix.Count;
            nColumns = matrix[0].Count;
        }

        public void WriteToFile(string filePath)
        {
            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filePath))
                {
                    foreach (List<double> row in matrix)
                    {
                        foreach (double value in row)
                        {
                            file.Write(value + " ");
                        }
                        file.WriteLine();
                    }
                }

                Console.WriteLine("File successfully written.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while trying to write the file.");
            }
        }

        public void PrintToScreen()
        {
            foreach (List<double> row in matrix)
            {
                foreach (double value in row)
                {
                    Console.Write(value + " ");
                }
                Console.WriteLine();
            }
        }

        public int GetNumberOfRows()
        {
            return this.nRows;
        }

        public int GetNumberOfColumns()
        {
            return this.nColumns;
        }

        public double GetValueAt(int rowIndex, int columnIndex)
        {
            return matrix[rowIndex][columnIndex];
        }

        public void SetValueAt(int rowIndex, int columnIndex, double value)
        {
            matrix[rowIndex][columnIndex] = value;
        }

        public static Matrix operator +(Matrix first, Matrix second)
        {
            Matrix returnMatrix = new Matrix(first.nRows, first.nColumns);

            if (first.nRows != second.nRows && first.nColumns != second.nColumns)
            {
                throw new ArgumentException();
            }

            for (int i = 0; i < first.nRows; i++)
            {
                for (int j = 0; j < first.nColumns; j++)
                {
                    returnMatrix.SetValueAt(i, j, first.GetValueAt(i, j) + second.GetValueAt(i, j));
                }
            }

            return returnMatrix;
        }

        public static Matrix operator -(Matrix first, Matrix second)
        {
            Matrix returnMatrix = new Matrix(first.nRows, first.nColumns);

            if (first.nRows != second.nRows && first.nColumns != second.nColumns)
            {
                throw new ArgumentException();
            }

            for (int i = 0; i < first.nRows; i++)
            {
                for (int j = 0; j < first.nColumns; j++)
                {
                    returnMatrix.SetValueAt(i, j, first.GetValueAt(i, j) - second.GetValueAt(i, j));
                }
            }

            return returnMatrix;
        }

        public static Matrix operator *(Matrix first, Matrix second)
        {
            Matrix returnMatrix = new Matrix(first.nRows, second.nColumns);

            if (first.nColumns != second.nRows)
            {
                throw new ArgumentException();
            }

            for (int i = 0; i < first.nRows; i++)
            {
                for (int j = 0; j < second.nColumns; j++)
                {
                    double sum = 0;
                    for (int k = 0; k < first.nColumns; k++)
                    {
                        sum += first.GetValueAt(i, k) * second.GetValueAt(k, j);
                    }
                    returnMatrix.SetValueAt(i, j, sum);
                }
            }

            return returnMatrix;
        }

        public static Matrix operator *(double scalar, Matrix mat)
        {
            return mat.MultiplyWithScalar(scalar);
        }

        public static Matrix operator *(Matrix mat, double scalar)
        {
            return mat.MultiplyWithScalar(scalar);
        }

        private Matrix MultiplyWithScalar(double scalar)
        {
            Matrix returnMatrix = new Matrix(this.nRows, this.nColumns);

            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nColumns; j++)
                {
                    returnMatrix.SetValueAt(i, j, this.GetValueAt(i, j) * scalar);
                }
            }

            return returnMatrix;
        }

        public static bool operator ==(Matrix first, Matrix second)
        {
            if (first != null && (second != null && (first.nRows == second.nRows && first.nColumns == second.nColumns)))
            {
                for (int i = 0; i < first.nRows; i++)
                {
                    for (int j = 0; j < first.nColumns; j++)
                    {
                        if ((first.GetValueAt(i, j) - second.GetValueAt(i, j)) > 0.00001)
                        {
                            return false;
                        }
                    }
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        public static bool operator !=(Matrix first, Matrix second)
        {
            try
            {
                if ((first.nRows == second.nRows && first.nColumns == second.nColumns))
                {
                    for (int i = 0; i < first.nRows; i++)
                    {
                        for (int j = 0; j < first.nColumns; j++)
                        {
                            if ((first.GetValueAt(i, j) - second.GetValueAt(i, j)) > 0.00001)
                            {
                                return true;
                            }
                        }
                    }
                }
                else
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Matrix operator ~(Matrix matrix)
        {
            Matrix returnMatrix = new Matrix(matrix.nColumns, matrix.nRows);

            for (int i = 0; i < matrix.nRows; i++)
            {
                for (int j = 0; j < matrix.nColumns; j++)
                {
                    returnMatrix.SetValueAt(j, i, matrix.GetValueAt(i, j));
                }
            }
            return returnMatrix;
        }

        public double this[int row, int column]
        {
            get { return this.GetValueAt(row, column); }
            set
            {
                this.SetValueAt(row, column, value);
            }
        }

        public void SubstituteForward(Matrix mat)
        {
            for (int i = 0; i < nColumns - 1; i++)
            {
                for (int j = i + 1; j < nColumns; j++)
                {
                    mat[0, j] -= this[j, i]*mat[0, i];
                }
            }
        }

        public Matrix Inverse() {
            Matrix inverse = new Matrix(this.GetNumberOfRows(), this.GetNumberOfColumns());

            for(int i = 0; i < this.GetNumberOfColumns(); i++) {
                Matrix temp = new Matrix(this);
                Matrix b = CreateNullColumnWithOne(this.GetNumberOfRows(), i);

                temp.LU(b, true);
                temp.SubstituteForward(b);
                temp.SubstituteBackward(b);

                for(int j = 0; j < this.GetNumberOfRows(); j++)
                {
                    inverse[j,i] = b[0, j];
                }
            }                

            return inverse;
        }

        public static Matrix CreateIdentityMatrix(int dimension)
        {
            Matrix mat = new Matrix(dimension, dimension);

            for(int i = 0; i < dimension; i++)
            {
                for(int j = 0; j < dimension; j++)
                {
                    mat[i,j] = 0;
                }

                mat[i,i] = 1;
            }

            return mat;
        }

        private Matrix CreateNullColumnWithOne(int nRows, int oneIndex) {
            Matrix mat = new Matrix(1, nRows);

            for(int i = 0; i < nRows; i++)
            {
                mat[0,i ] = 0;
            }

            mat[0, oneIndex] = 1;

            return mat;
        }

        public Matrix SubstituteBackward(Matrix mat)
        {
            for (int i = nColumns - 1; i >= 0; i--)
            {
                mat[0, i] /= this[i, i];
                for (int j = 0; j < i; j++)
                {
                    mat[0, j] -= this[j, i] * mat[0, i];
                }
            }

            return mat;
        }

        public Matrix LU(Matrix vect = null,bool LUP = false)
        {
            for (int i = 0; i < nColumns - 1; i++)
            {
                if (LUP)
                {
                    int pivot = getPivotElementRowIndex(i);
                    if ((Math.Abs(this[pivot, i]) < 10e-6))
                    {
                        Console.WriteLine("Matrica je singularna");
                        Environment.Exit(1);
                    }

                    if (pivot != i)
                    {
                        switchRows(pivot, i, vect);
                    }
                }
                for (int j = i + 1; j < nColumns; j++)
                {
                    if ((Math.Abs(this[i, i]) < 10e-6))
                    {
                        Console.WriteLine("Pivot je 0.");
                        Environment.Exit(1);
                    }
                    this[j, i] /= this[i, i];
                    for (int k = i + 1; k < nColumns; k++)
                    {
                        this[j, k] -= this[j, i] * this[i, k];
                    }
                }
            }

            if ((Math.Abs(this[nRows - 1, nColumns - 1])) < 10e-6)
            {
                Console.WriteLine("Sustav nema rješenja!");
                //Environment.Exit(1);
            }

            return this;
        }

        private void switchRows(int first, int second, Matrix vect)
        {
            var temp = this.matrix[first];
            this.matrix[first] = this.matrix[second];
            this.matrix[second] = temp;
            var temp1 = vect[0, first];
            vect[0, first] = vect[0, second];
            vect[0, second] = temp1;
        }

        private int getPivotElementRowIndex(int current)
        {
            int pivot = current;
            for (int j = current + 1; j < nRows; j++)
            {
                if (Math.Abs(this[j, current]) > Math.Abs(this[pivot, current]))
                {
                    pivot = j;
                }
            }
            return pivot;
        }

        private List<double> GetMatrixRow(int j)
        {
            List<double> column = new List<double>();
            for (int i = 0; i < this.nRows; i++)
            {
                column.Add(this.matrix[i][j]);
            }
            return column;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (List<double> row in matrix)
            {
                foreach (double value in row)
                {
                    sb.Append(value + " ");
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Matrix))
            {
                return false;
            }

            Matrix ob = obj as Matrix;
            if (ob.nRows != this.nRows || ob.nColumns != this.nColumns)
            {
                return false;
            }

            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nColumns; j++)
                {
                    if ( Math.Abs(this[i, j] - ob[i, j]) > 1e-6 )
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hash = 13;

            for (int i = 0; i < nRows; i++)
            {
                for (int j = 0; j < nColumns; j++)
                {
                    hash = (hash*7) + this[i, j].GetHashCode();
                }
            }

            return hash;
        }
    }
}
