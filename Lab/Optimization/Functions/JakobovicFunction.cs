﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Functions
{
    public class JakobovicFunction : AbstractFunction
    {
        protected override double callFunction(Matrix x)
        {
            double x1 = x[0, 0];
            double x2 = x[0, 1];

            double val = Math.Abs((x1 - x2) * (x1 + x2)) + Math.Sqrt(x1 * x1 + x2 * x2);
            return val;
        }

        protected override double CalculateFunctionValue(double x)
        {
            double x1 = xs[0, 0] + x * e[0, 0];
            double x2 = xs[0, 1] + x * e[0, 1];

            double val = Math.Abs((x1 - x2)*(x1 + x2)) + Math.Sqrt(x1*x1 + x2*x2);
            return val;
        }

        public override Matrix GetGradientInPoint(Matrix point)
        {
            throw new NotImplementedException();
        }

        protected override Matrix GetHMatrix(Matrix point)
        {
            throw new NotImplementedException();
        }

        public override int NumberOfVariables()
        {
            throw new NotImplementedException();
        }
    }
}
