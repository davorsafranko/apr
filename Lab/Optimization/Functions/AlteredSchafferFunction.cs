﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Functions
{
    public class AlteredSchafferFunction : AbstractFunction
    {
        private int variables;

        public AlteredSchafferFunction(int variables)
        {
            this.variables = variables;
        }

        protected override double callFunction(Matrix x)
        {
            double quadraticSum = 0;

            for (int i = 0; i < x.GetNumberOfColumns(); i++)
            {
                quadraticSum += x[0, i]*x[0, i];
            }

            double firstPart = Math.Pow(quadraticSum, 0.25);
            double secondPart = 50*Math.Pow(quadraticSum, 0.1);
            secondPart = Math.Pow(Math.Sin(secondPart), 2) + 1;

            return firstPart * secondPart;
        }

        protected override double CalculateFunctionValue(double x)
        {
            throw new NotImplementedException();
        }

        public override Matrix GetGradientInPoint(Matrix point)
        {
            throw new NotImplementedException();
        }

        protected override Matrix GetHMatrix(Matrix point)
        {
            throw new NotImplementedException();
        }

        public override int NumberOfVariables()
        {
            return variables;
        }
    }
}
