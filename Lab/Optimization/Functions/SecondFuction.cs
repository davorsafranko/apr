﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Functions
{
    public class SecondFuction : AbstractFunction
    {
        protected override double callFunction(Matrix x)
        {
            double x1 = x[0, 0];
            double x2 = x[0, 1];

            double val = (x1 - 4) * (x1 - 4) + 4 * (x2 - 2) * (x2 - 2);
            return val;
        }

        protected override double CalculateFunctionValue(double x)
        {
            double x1 = xs[0, 0] + x * e[0, 0];
            double x2 = xs[0, 1] + x * e[0, 1];

            double val = (x1 - 4)*(x1 - 4) + 4*(x2 - 2)*(x2 - 2);
            return val;
        }

        public override Matrix GetGradientInPoint(Matrix point)
        {
            double x1 = point[0, 0];
            double x2 = point[0, 1];

            double gradientByFirstVariable = 2*(x1 - 4);
            double gradientBySecondVariable = 8*(x2 - 2);

            Matrix gradient = new Matrix(point);
            gradient[0, 0] = gradientByFirstVariable;
            gradient[0, 1] = gradientBySecondVariable;

            return gradient;
        }

        protected override Matrix GetHMatrix(Matrix point)
        {
            Matrix hesseMatrix = new Matrix(2, 2);
            hesseMatrix[0, 0] = 2;
            hesseMatrix[0, 1] = 0;
            hesseMatrix[1, 0] = 0;
            hesseMatrix[1, 1] = 8;
            return hesseMatrix;
        }

        public override int NumberOfVariables()
        {
            throw new NotImplementedException();
        }
    }
}
