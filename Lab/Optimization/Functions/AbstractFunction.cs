﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization
{
    public abstract class AbstractFunction
    {
        private int _numberOfCalls = 0;
        private int gradientCalc = 0;
        private int hesseCalc = 0;
        protected Matrix xs;
        protected Matrix e;

        public double call(double x)
        {
            //Matrix newM = new Matrix(1, xs.GetNumberOfColumns());

            //for (int i = 0; i < xs.GetNumberOfColumns(); i++)
            //{
            //    newM[0, i] = xs[0, i] + x * e[0, i];
            //}

            double value = CalculateFunctionValue(x);
            _numberOfCalls++;

            return value;
        }

        public abstract int NumberOfVariables();

        public double call(Matrix x)
        {
            double value = callFunction(x);
            _numberOfCalls++;

            return value;
        }

        protected abstract double callFunction(Matrix x);

        protected abstract double CalculateFunctionValue(double x);

        public abstract Matrix GetGradientInPoint(Matrix point);

        public Matrix GetHesseMatrix(Matrix point)
        {
            hesseCalc++;
            return GetHMatrix(point);
        }

        protected abstract Matrix GetHMatrix(Matrix point);

        public void SetVectors(Matrix xs, Matrix e)
        {
            this.xs = xs;
            this.e = e;
        }

        public void ResetNumberOfCalls()
        {
            this._numberOfCalls = 0;
        }

        public int GetNumberOfCalls()
        {
            return this._numberOfCalls;
        }

        public void SubtractNumberOfCalls(int amount)
        {
            this._numberOfCalls -= amount;
        }

        public int GetHesseCalls()
        {
            return this.hesseCalc;
        }
    }
}
