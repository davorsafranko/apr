﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization
{
    public class RosenbrockFunction : AbstractFunction
    {
        protected override double callFunction(Matrix x)
        {
            double x1 = x[0, 0];
            double x2 = x[0, 1];

            double val = 100 * (x2 - x1 * x1) * (x2 - x1 * x1) + (1 - x1) * (1 - x1);
            return val;
        }

        protected override double CalculateFunctionValue(double x)
        {
            double x1 = xs[0, 0] + x*e[0, 0];
            double x2 = xs[0, 1] + x*e[0, 1];

            double val = 100*(x2 - (x1*x1))*(x2 - (x1*x1)) + ((1 - x1)*(1 - x1));
            return val;
        }

        public override Matrix GetGradientInPoint(Matrix point)
        {
            double x1 = point[0, 0];
            double x2 = point[0, 1];

            double gradientByFirstVariable = -(200*2*x1*(x2 - x1*x1) + 2*(1 - x1));
            double gradientBySecondVariable = 200*(x2-x1*x1);

            Matrix gradient = new Matrix(point);
            gradient[0, 0] = gradientByFirstVariable;
            gradient[0, 1] = gradientBySecondVariable;

            return gradient;
        }

        protected override Matrix GetHMatrix(Matrix point)
        {
            double x1 = point[0, 0];
            double x2 = point[0, 1];

            Matrix hesseMatrix = new Matrix(2, 2);
            hesseMatrix[0, 0] = -400*x2 + 1200*x1*x1 + 2;
            hesseMatrix[0, 1] = -400*x1;
            hesseMatrix[1, 0] = -400*x1;
            hesseMatrix[1, 1] = 200;
            return hesseMatrix;
        }

        public override int NumberOfVariables()
        {
            return 2;
        }
    }
}
