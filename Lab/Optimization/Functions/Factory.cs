﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab.Optimization.Constraints;

namespace Lab.Optimization.Functions
{
    public class Factory
    {

        public static AbstractFunction GetFunction(List<ImplicitConstraint> implicitConstraints)
        {
            return new ABFunction(implicitConstraints);
        }

        public static AbstractFunction GetUFunction(List<ImplicitConstraint> implicitConstraints,
            EqualityConstraint constraint, AbstractFunction f, double t)
        {
            return new FunctionAllConstraints(implicitConstraints, constraint, f, t);
        }

        private class ABFunction : AbstractFunction
        {
            private List<ImplicitConstraint> constraints;

            public ABFunction(List<ImplicitConstraint> implConstraints)
            {
                this.constraints = implConstraints;
            } 

            protected override double callFunction(Matrix x)
            {
                double sum = 0;

                foreach (ImplicitConstraint constraint in constraints)
                {
                    if (!constraint.IsSatisfied(x))
                    {
                        sum += constraint.GetValue(x);
                    }
                }

                return -sum;
            }

            protected override double CalculateFunctionValue(double x)
            {
                throw new NotImplementedException();
            }

            public override Matrix GetGradientInPoint(Matrix point)
            {
                throw new NotImplementedException();
            }

            protected override Matrix GetHMatrix(Matrix point)
            {
                throw new NotImplementedException();
            }

            public override int NumberOfVariables()
            {
                throw new NotImplementedException();
            }
        }

        private class FunctionAllConstraints : AbstractFunction
        {
            private List<ImplicitConstraint> constraints;
            private EqualityConstraint explicitConstraint;
            private AbstractFunction F;
            private double t;

            public FunctionAllConstraints(List<ImplicitConstraint> implicitConstraints, EqualityConstraint explicitConstraint, AbstractFunction F, double t)
            {
                this.constraints = implicitConstraints;
                this.explicitConstraint = explicitConstraint;
                this.F = F;
                this.t = t;
            }

            protected override double callFunction(Matrix x)
            {
                double sum = 0;

                sum += F.call(x);

                foreach (ImplicitConstraint constraint in constraints)
                {
                    sum -= (1.0/t)*Math.Log(constraint.GetValue(x));
                }

                if (explicitConstraint != null)
                {
                    sum += t * Math.Pow(explicitConstraint.GetValue(x), 2);
                }

                return sum;
            }

            protected override double CalculateFunctionValue(double x)
            {
                throw new NotImplementedException();
            }

            public override Matrix GetGradientInPoint(Matrix point)
            {
                throw new NotImplementedException();
            }

            protected override Matrix GetHMatrix(Matrix point)
            {
                throw new NotImplementedException();
            }

            public override int NumberOfVariables()
            {
                throw new NotImplementedException();
            }
        }
    }
}
