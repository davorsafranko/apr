﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Functions
{
    public class F3 : AbstractFunction
    {
        protected override double callFunction(Matrix x)
        {
            double value = 0;
            for (int i = 0; i < x.GetNumberOfColumns(); i++)
            {
                value += (x[0, i] - (i + 1)) * (x[0, i] - (i + 1));
            }

            return value;
        }

        protected override double CalculateFunctionValue(double x)
        {
            Matrix newM = new Matrix(this.xs);
            for (int i = 0; i < newM.GetNumberOfColumns(); i++)
            {
                newM[0, i] += x*this.e[0, i];
            }

            double value = 0;
            for (int i = 0; i < newM.GetNumberOfColumns(); i++)
            {
                value += (newM[0, i] - (i+1))*(newM[0, i] - (i+1));
            }

            return value;
        }

        public override Matrix GetGradientInPoint(Matrix point)
        {
            throw new NotImplementedException();
        }

        protected override Matrix GetHMatrix(Matrix point)
        {
            throw new NotImplementedException();
        }

        public override int NumberOfVariables()
        {
            return 5;
        }
    }
}
