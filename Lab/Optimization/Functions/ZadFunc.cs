﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Functions
{
    public class ZadFunc : AbstractFunction
    {
        protected override double callFunction(Matrix x)
        {
            return Math.Pow(x[0,0], 2) + 2*Math.Pow(x[0,1], 2);
        }

        protected override double CalculateFunctionValue(double x)
        {
            double x1 = xs[0, 0] + x * e[0, 0];
            double x2 = xs[0, 1] + x * e[0, 1];

            double val = Math.Pow(x1, 2) + 2 * Math.Pow(x2, 2);
            return val;
        }

        public override Matrix GetGradientInPoint(Matrix point)
        {
            double x1 = point[0, 0];
            double x2 = point[0, 1];

            double gradientByFirstVariable = 2 * (x1);
            double gradientBySecondVariable = 4 * (x2);

            Matrix gradient = new Matrix(point);
            gradient[0, 0] = gradientByFirstVariable;
            gradient[0, 1] = gradientBySecondVariable;

            return gradient;
        }

        protected override Matrix GetHMatrix(Matrix point)
        {
            throw new NotImplementedException();
        }

        public override int NumberOfVariables()
        {
            throw new NotImplementedException();
        }
    }
}
