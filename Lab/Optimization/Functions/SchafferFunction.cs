﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Functions
{
    public class SchafferFunction : AbstractFunction
    {
        private int variables;

        public SchafferFunction(int variables)
        {
            this.variables = variables;
        }

        protected override double callFunction(Matrix x)
        {
            double sum = 0;
            for (int i = 0; i < x.GetNumberOfColumns(); i++)
            {
                sum += Math.Pow(x[0, i], 2);
            }

            double sin = Math.Sin(Math.Sqrt(sum));
            double br = 1 + 0.001 * sum;
            double value = 0.5 + (sin * sin - 0.5) / (br * br);

            return value;
        }

        protected override double CalculateFunctionValue(double x)
        {
            Matrix newM = new Matrix(this.xs);
            for (int i = 0; i < newM.GetNumberOfColumns(); i++)
            {
                newM[0, i] += x * this.e[0, i];
            }

            double sum = 0;
            for (int i = 0; i < newM.GetNumberOfColumns(); i++)
            {
                sum += Math.Pow(newM[0, i], 2);
            }

            double sin = Math.Sin(Math.Sqrt(sum));
            double br = 1 + 0.001*sum;
            double value = 0.5 + (sin*sin - 0.5)/(br*br);

            return value;
        }

        public override Matrix GetGradientInPoint(Matrix point)
        {
            throw new NotImplementedException();
        }

        protected override Matrix GetHMatrix(Matrix point)
        {
            throw new NotImplementedException();
        }

        public override int NumberOfVariables()
        {
            return variables;
        }
    }
}
