﻿using Lab.Optimization.EvolutionaryAlgorithms.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.EvolutionaryAlgorithms
{
    public class Selections
    {
        private static Random rand;

        public Selections(Random random){
            rand = random;
        }


        public static List<IUnit> rouleteWheelSelect(List<IUnit> population) {
            List<IUnit> parents = new List<IUnit>();

            List<IUnit> thisP = new List<IUnit>(population);

            double sumOfFitness = CalculateFitnessSum(thisP);
            parents.Add(PickParentRouletteWheel(thisP, sumOfFitness));

            sumOfFitness = CalculateFitnessSum(thisP);
            parents.Add(PickParentRouletteWheel(thisP, sumOfFitness));

            return parents;
        }

        public static List<IUnit> kTournamentSelect(List<IUnit> population) {
            
            return null;
        }

        private static IUnit PickParentRouletteWheel(List<IUnit> population, double sumOfFitness)
        {
            double pickElement = rand.NextDouble() * sumOfFitness;

            double sum = 0;
            foreach (IUnit unit in population)
            {
                sum += unit.GetFitness();
                if (sum > pickElement)
                {
                    population.Remove(unit);
                    return unit;
                }
            }
            return null;
        }

        private static double CalculateFitnessSum(List<IUnit> population)
        {
            double sumOfFitness = 0;
            foreach (IUnit unit in population)
            {
                sumOfFitness += unit.GetFitness();
            }

            return sumOfFitness;
        }
    }
}
