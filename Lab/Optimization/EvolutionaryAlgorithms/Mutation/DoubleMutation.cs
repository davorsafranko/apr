﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab.Optimization.EvolutionaryAlgorithms.Units;

namespace Lab.Optimization.EvolutionaryAlgorithms.Mutation
{
    public class DoubleMutation : IMutation
    {
        private static Random _randomGenerator;
        private static double _mutationProbability;
        private static double _scale;
        private double _y2;
        private bool _generate = false;
        private static double _sigma;

        public DoubleMutation(Random randomGenerator, double mutationProbability, double sigma,double scale)
        {
            _randomGenerator = randomGenerator;
            _mutationProbability = mutationProbability;
            _scale = scale;
            _sigma = sigma;
        }

        public IUnit Mutate(IUnit unit)
        {
            if (unit.GetType() != typeof(DoubleUnit))
            {
                throw new ArgumentException();
            }

			DoubleUnit thisUnit = unit as DoubleUnit;

            if (thisUnit != null)
            {
                var variables = thisUnit.GetVariables();

                for (int i = 0; i < variables.Count; i++)
                {
                    double rand = _randomGenerator.NextDouble();
                    if (rand < _mutationProbability)
                    {
                        double value = GenerateGaussianNoise();
                        variables[i] = variables[i] + value;

                        if (variables[i] > thisUnit.GetUpperBound())
                        {
                            variables[i] = thisUnit.GetUpperBound();
                        }
                        else if (variables[i] <= thisUnit.GetLowerBound())
                        {
                            variables[i] = thisUnit.GetLowerBound();
                        }
                    }
                }
            }

            return thisUnit;
        }

        private double GenerateGaussianNoise(double mu = 0)
        {
            double y1;

            if (_generate)
            {
                y1 = _y2;
                _generate = false;
            }
            else
            {
                double x1;
                double x2;
                double w;
                do
                {
                    x1 = 2*_randomGenerator.NextDouble() - 1;
                    x2 = 2 * _randomGenerator.NextDouble() - 1;

                    w = x1*x1 + x2*x2;
                } while (w >= 1.0);

                w = Math.Sqrt((-2.0*Math.Log(w))/w);
                y1 = x1*w;
                _y2 = x2*w;
                _generate = true;
            }

            return _scale*(mu + y1 * _sigma);
        }
    }
}
