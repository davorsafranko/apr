﻿using Lab.Optimization.EvolutionaryAlgorithms.Units;

namespace Lab.Optimization.EvolutionaryAlgorithms.Mutation
{
    public interface IMutation
    {
        IUnit Mutate(IUnit unit);
    }
}
