﻿using System;
using Lab.Optimization.EvolutionaryAlgorithms.Units;

namespace Lab.Optimization.EvolutionaryAlgorithms.Mutation
{
    public class BinaryMutation : IMutation
    {
        private static double _mutationProbability;
        private readonly Random _randomNumberGenerator;

        public BinaryMutation(Random randomNumberGenerator, double mutationProbability)
        {
            _mutationProbability = mutationProbability;
            _randomNumberGenerator = randomNumberGenerator;
        }

        public IUnit Mutate(IUnit unit)
        {
            if (unit.GetType() != typeof (BinaryUnit))
            {
                throw new ArgumentException();
            }

            BinaryUnit thisUnit = unit as BinaryUnit;

            for (int i = 0; i < thisUnit.GetVariableNumber(); i++)
            {
                for (int j = 0; j < thisUnit.ChromosomeLength(); j++)
                {
                    double rand = _randomNumberGenerator.NextDouble();
                    if (rand < _mutationProbability)
                    {
                        thisUnit.SetValueAt(i,j, 1 - thisUnit.GetValueAt(i,j));
                    }
                }
            }

            return thisUnit;
        }
    }
}
