﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab.Optimization.EvolutionaryAlgorithms.Units;

namespace Lab.Optimization.EvolutionaryAlgorithms.Crossover
{
    public class DoubleUniformCrossover : ICrossover
    {
        private readonly Random _randomNumberGenerator;
        private readonly double _crossOverProbability;

        public DoubleUniformCrossover(Random randomNumberGenerator, double crossOverProbability)
        {
            _randomNumberGenerator = randomNumberGenerator;
            _crossOverProbability = crossOverProbability;
        }

        public IUnit Crossover(IUnit firstParent, IUnit secondParent)
        {
            DoubleUnit child = new DoubleUnit(firstParent as DoubleUnit);

            for (int i = 0; i < ((DoubleUnit) firstParent).GetVariables().Count; i++)
            {
                double rand = _randomNumberGenerator.NextDouble();
                if (rand <= _crossOverProbability)
                {
                    child.SetValueAt(i, ((DoubleUnit)secondParent).GetValueOfVariable(i));
                }
            }

            return child;
        }
    }
}
