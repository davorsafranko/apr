﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab.Optimization.EvolutionaryAlgorithms.Units;

namespace Lab.Optimization.EvolutionaryAlgorithms.Crossover
{
    public class BinaryOnePointCrossover : ICrossover
    {
        private readonly Random _randomNumberGenerator;

        public BinaryOnePointCrossover(Random randomNumberGenerator)
        {
            _randomNumberGenerator = randomNumberGenerator;
        }

        public IUnit Crossover(IUnit firstParent, IUnit secondParent)
        {
            BinaryUnit child = new BinaryUnit(firstParent as BinaryUnit);

            for (int j = 0; j < firstParent.GetVariableNumber(); j++)
            {
                int point = _randomNumberGenerator.Next(1, child.ChromosomeLength() - 1);

                for (int i = 0; i < ((BinaryUnit)firstParent).ChromosomeLength(); i++)
                {
                    if (point < i)
                    {
                        child.SetValueAt(j, i, ((BinaryUnit) firstParent).GetValueAt(j, i));
                    }
                    else
                    {
                        child.SetValueAt(j, i, ((BinaryUnit)secondParent).GetValueAt(j, i));
                    }
                }
            }

            return child;
        }
    }
}
