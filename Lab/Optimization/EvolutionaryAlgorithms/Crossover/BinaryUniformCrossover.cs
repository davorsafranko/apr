﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab.Optimization.EvolutionaryAlgorithms.Units;

namespace Lab.Optimization.EvolutionaryAlgorithms.Crossover
{
    public class BinaryUniformCrossover : ICrossover
    {
        private readonly Random _randomNumberGenerator;
        private readonly double _crossOverProbability;

        public BinaryUniformCrossover(Random randomNumberGenerator, double crossOverProbability)
        {
            _randomNumberGenerator = randomNumberGenerator;
            _crossOverProbability = crossOverProbability;
        }
        public IUnit Crossover(IUnit firstParent, IUnit secondParent)
        {
            BinaryUnit child = new BinaryUnit((BinaryUnit)firstParent);

            for (int j = 0; j < firstParent.GetVariableNumber(); j++)
            {
                for (int i = 0; i < ((BinaryUnit)firstParent).ChromosomeLength(); i++)
                {
                    double rand = _randomNumberGenerator.NextDouble();
                    if (rand >= _crossOverProbability)
                    {
                        child.SetValueAt(j,i, ((BinaryUnit)secondParent).GetValueAt(j,i));
                    }
                }
            }

            return child;
        }
    }
}
