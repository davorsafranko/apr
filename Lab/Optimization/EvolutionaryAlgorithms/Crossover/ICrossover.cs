﻿using Lab.Optimization.EvolutionaryAlgorithms.Units;

namespace Lab.Optimization.EvolutionaryAlgorithms.Crossover
{
    public interface ICrossover
    {
        IUnit Crossover(IUnit firstParent, IUnit secondParent);
    }
}
