﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab.Optimization.EvolutionaryAlgorithms.Units;

namespace Lab.Optimization.EvolutionaryAlgorithms.Crossover
{
    public class DoubleArithmeticCrossover : ICrossover
    {
        private readonly Random _randomNumberGenerator;

        public DoubleArithmeticCrossover(Random randomNumberGenerator)
        {
            _randomNumberGenerator = randomNumberGenerator;
        }

        public IUnit Crossover(IUnit firstParent, IUnit secondParent)
        {
            DoubleUnit child = new DoubleUnit(firstParent as DoubleUnit);

            for (int i = 0; i < ((DoubleUnit)firstParent).GetVariables().Count; i++)
            {
                double rand = _randomNumberGenerator.NextDouble();
                child.SetValueAt(i, rand * ((DoubleUnit)firstParent).GetValueOfVariable(i) + (1 - rand) * ((DoubleUnit)secondParent).GetValueOfVariable(i));
            }

            return child;
        }
    }
}
