﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.EvolutionaryAlgorithms.Units
{
    public class BinaryUnit : IUnit
    {
        private readonly List<List<int>> _variables;
        private readonly int _lowerBound;
        private readonly int _upperBound;
        private readonly Random _randomGenerator;
        private readonly int _chromosomeLength;
        private readonly int _variableNumber;
        private double _fitness;

        public BinaryUnit(int lowerBound, int upperBound, double precision, int variableNumber, Random randomGenerator)
        {
            _variables = new List<List<int>>(variableNumber);
            this._lowerBound = lowerBound;
            this._upperBound = upperBound;
            this._variableNumber = variableNumber;

            _randomGenerator = randomGenerator;
            _chromosomeLength = GetChromosomeLength(precision);
            GenerateUnit(variableNumber, _chromosomeLength);
        }

        public BinaryUnit(BinaryUnit unit)
        {
            _variables = new List<List<int>>(unit._variableNumber);
            this._lowerBound = unit._lowerBound;
            this._upperBound = unit._upperBound;

            _randomGenerator = unit._randomGenerator;
            _chromosomeLength = unit._chromosomeLength;
            _variableNumber = unit._variableNumber;

            for (int i = 0; i < unit._variableNumber; i++)
            {
                var chromosome = new List<int>(unit._chromosomeLength);
                for (int j = 0; j < unit._chromosomeLength; j++)
                {
                    chromosome.Add(unit._variables[i][j]);
                }

                _variables.Add(chromosome);
            }
        }

        private void GenerateUnit(int variableNumber, int chromosomeLength)
        {
            for (int i = 0; i < variableNumber; i++)
            {
                var chromosome = new List<int>(chromosomeLength);
                for (int j = 0; j < chromosomeLength; j++)
                {
                    if (_randomGenerator.NextDouble() < 0.5)
                    {
                        chromosome.Add(0);
                    }
                    else
                    {
                        chromosome.Add(1);
                    }
                }
                _variables.Add(chromosome);
            }
        }

        public double GetValueOfVariable(int index)
        {
            double value = 0;

            int multi = 1;
            for (int i = _chromosomeLength - 1; i >= 0; i--)
            {
                value += _variables[index][i]*multi;
                multi *= 2;
            }

            return _lowerBound + (value / (Math.Pow(2,_chromosomeLength) - 1)) * (_upperBound - _lowerBound);
        }

        public int GetValueAt(int variable, int index)
        {
            return _variables[variable][index];
        }

        public void SetValueAt(int variable, int index, int value)
        {
            _variables[variable][index] = value;
        }

        public double GetLowerBound()
        {
            return _lowerBound;
        }

        public double GetUpperBound()
        {
            return _upperBound;
        }

        public int GetVariableNumber()
        {
            return this._variableNumber;
        }

        public void CalculateFitness(AbstractFunction function)
        {
            Matrix point = new Matrix(1, _variables.Capacity);
            for (int i = 0; i < _variables.Count; i++)
            {
                point[0, i] = GetValueOfVariable(i);
            }

            _fitness = 1.0 / function.call(point);
        }

        public double GetFitness()
        {
            return _fitness;
        }

        private int GetChromosomeLength(double precision)
        {
            double firstValue = 1 + (_upperBound - _lowerBound)*Math.Pow(10, precision);
            double n = Math.Log10(Math.Floor(firstValue))/Math.Log10(2);

            return (int) Math.Ceiling(n);
        }

        public int ChromosomeLength()
        {
            return this._chromosomeLength;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("(");
            for (int i = 0; i < _variables.Count - 1; i++)
            {
                sb.Append(GetValueOfVariable(i) + ", ");
            }

            sb.Append(GetValueOfVariable(_variables.Count - 1));
            sb.Append(")");
            return sb.ToString();
        }
    }
}
