﻿namespace Lab.Optimization.EvolutionaryAlgorithms.Units
{
    public interface IUnit
    {
        double GetValueOfVariable(int index);
        double GetLowerBound();
        double GetUpperBound();
        int GetVariableNumber();
        void CalculateFitness(AbstractFunction function);
        double GetFitness();
    }
}
