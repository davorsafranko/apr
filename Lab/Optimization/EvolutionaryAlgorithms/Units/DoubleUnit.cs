﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.EvolutionaryAlgorithms.Units
{
    public class DoubleUnit : IUnit
    {
        private readonly List<double> _variables;
        private readonly int _lowerBound;
        private readonly int _upperBound;
        private readonly Random _randomGenerator;
        private readonly int _variableNumber;
        private double _fitness;

        public DoubleUnit(int lowerBound, int upperBound, int variableNumber, Random randomGenerator)
        {
            _variables = new List<double>(variableNumber);
            this._lowerBound = lowerBound;
            this._upperBound = upperBound;
            _variableNumber = variableNumber;

            _randomGenerator = randomGenerator;
            GenerateUnit(variableNumber);
        }

        public DoubleUnit(DoubleUnit unit)
        {
            _variables = new List<double>(unit._variableNumber);
            _lowerBound = unit._lowerBound;
            _upperBound = unit._upperBound;

            _randomGenerator = unit._randomGenerator;

            foreach (double variable in unit._variables)
            {
                _variables.Add(variable);
            }
        }

        private void GenerateUnit(int variableNumber)
        {
            for (int i = 0; i < variableNumber; i++)
            {
                double rand = _randomGenerator.NextDouble();
                _variables.Add( _lowerBound + (_upperBound - _lowerBound) * rand );
            }
        }

        public double GetValueOfVariable(int index)
        {
            return _variables[index];
        }

        public void SetValueAt(int index, double value)
        {
            if (value > _upperBound)
            {
                value = _upperBound;
            }else if (value < _lowerBound)
            {
                value = _lowerBound;
            }

            _variables[index] = value;
        }

        public double GetLowerBound()
        {
            return _lowerBound;
        }

        public double GetUpperBound()
        {
            return _upperBound;
        }

        public int GetVariableNumber()
        {
            return _variableNumber;
        }

        public void CalculateFitness(AbstractFunction function)
        {
            Matrix point = new Matrix(1, _variables.Count);
            for (int i = 0; i < _variables.Count; i++)
            {
                point[0, i] = _variables[i];
            }

            _fitness = 1.0/function.call(point);
        }

        public double GetFitness()
        {
            return _fitness;
        }

        public List<double> GetVariables()
        {
            return this._variables;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("(");
            for (int i = 0; i < _variables.Count - 1; i++)
            {
                sb.Append(GetValueOfVariable(i) + ", ");
            }

            sb.Append(GetValueOfVariable(_variables.Count - 1));
            sb.Append(")");
            return sb.ToString();
        }
    }
}
