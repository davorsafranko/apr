﻿using System;
using System.Collections.Generic;
using Lab.Optimization.EvolutionaryAlgorithms.Crossover;
using Lab.Optimization.EvolutionaryAlgorithms.Mutation;
using Lab.Optimization.EvolutionaryAlgorithms.Units;

namespace Lab.Optimization.EvolutionaryAlgorithms
{
    public class EvolutionaryAlgorithm
    {
        private int _populationSize;
        private readonly UnitType _unitType;
        private readonly int _numberOfIterations;
        private int _currentIteration = 0;
        private readonly int _lowerBound;
        private readonly int _upperBound;
        private readonly double _binaryPrecision = 0;
        private double _mutationProbability;
        private readonly double _crossOverProbability;
        private readonly IMutation _mutation;
        private readonly ICrossover _crossOver;
        private readonly AbstractFunction _function;
        private readonly Matrix _point;
        private readonly Random _randomGenerator;
        private List<IUnit> _units;
        private IUnit _previousBest = null;
        private readonly bool _printIterationInfo;
        private int _tournamentSize = 3;
        private int keep = 3;
        private Selections selections;

        public EvolutionaryAlgorithm(string parametersFileName, AbstractFunction function, Matrix point, bool printIterationInfo = true)
        {
            string[] lines = System.IO.File.ReadAllLines(parametersFileName);

            _populationSize = Int32.Parse(lines[0]);
            _unitType = (UnitType)Int32.Parse(lines[1]);
            _mutationProbability = Double.Parse(lines[2]);
            _crossOverProbability = Double.Parse(lines[3]);
            _numberOfIterations = Int32.Parse(lines[4]);
            _lowerBound = Int32.Parse(lines[5]);
            _upperBound = Int32.Parse(lines[6]);

            if (lines.Length == 8)
            {
                _binaryPrecision = Double.Parse(lines[7]);
            }

            Random randomGenerator = new Random();
            _randomGenerator = randomGenerator;

            IMutation mutation = GetMutation();
            ICrossover crossOver = GetCrossOver();

            _mutation = mutation;
            _crossOver = crossOver;
            _function = function;
            _point = point;
            _printIterationInfo = printIterationInfo;

            selections = new Selections(_randomGenerator);
        }

        public IUnit Start()
        {
            _units = CreatePopulation();

            do
            {
                for(int i = 0; i < _populationSize; i++) {
                    List<int> selectedUnits = new List<int>();
                    while (selectedUnits.Count < _tournamentSize)
                    {
                        int nextRand = _randomGenerator.Next(0, _populationSize);
                        if (!selectedUnits.Contains(nextRand))
                        {
                            selectedUnits.Add(nextRand);
                        }
                    }

                    int worstUnitIndex = GetWorstUnitIndex(selectedUnits);
                    selectedUnits.Remove(worstUnitIndex);

                    List<int> parents = SelectTwoParents(selectedUnits);
                    IUnit child = _crossOver.Crossover(_units[parents[0]], _units[parents[1]]);
                    child = _mutation.Mutate(child);
                    child.CalculateFitness(_function);

                    _units[worstUnitIndex] = child;
                    if (_previousBest == null)
                    {
                        _previousBest = GetBestUnit();
                    }
                    else
                    {
                        if (_previousBest.GetFitness() < child.GetFitness())
                        {
                            _previousBest = child;
                            if (_printIterationInfo)
                            {
                                PrintIterationInfo();
                            }
                        }
                    }
                }       
                _currentIteration++;
               
            } while (!ShouldStop());

            return GetBestUnit();
        }

        public IUnit GenerationalAlgorithm() {
            _units = CreatePopulation();

            do {
                List<IUnit> newPopulation = new List<IUnit>();

                List<IUnit> bestN = GetBestNUnits(this.keep);
                newPopulation.AddRange(bestN);

                while (newPopulation.Count < _populationSize)
                {
                    List<IUnit> parents = Selections.rouleteWheelSelect(_units);
                    IUnit child = _crossOver.Crossover(parents[0], parents[1]);
                    child = _mutation.Mutate(child);
                    child.CalculateFitness(_function);
                    newPopulation.Add(child);
                }

                newPopulation.Sort((u1, u2) => u2.GetFitness().CompareTo(u1.GetFitness()));
                this._units = newPopulation;

                if (_previousBest == null)
                {
                    _previousBest = newPopulation[0];
                }
                else
                {
                    if (_previousBest.GetFitness() < newPopulation[0].GetFitness())
                    {
                        _previousBest = newPopulation[0];
                        if (_printIterationInfo)
                        {
                            //PrintIterationInfo();
                        }
                    }
                }

                this._currentIteration++;
            } while(!ShouldStop());

            return GetBestUnit();
        }

        private List<IUnit> GetBestNUnits(int n)
        {
            List<IUnit> bestN = new List<IUnit>();

            for(int i = 0; i < n; i++) {
                bestN.Add(_units[i]);
            }

            return bestN;
        }

        private List<int> SelectTwoParents(List<int> units)
        {
            List<int> parents = new List<int>(2);

            while (parents.Count < 2)
            {
                int parent = _randomGenerator.Next(0, units.Count);
                if (!parents.Contains(parent))
                {
                    parents.Add(parent);
                }
            }

            return parents;
    } 

        private void PrintIterationInfo()
        {
            Console.WriteLine("Iteration : {0}, Current point : {1}, Fitness : {2}", _currentIteration, _previousBest, _previousBest.GetFitness());
        }

        private bool ShouldStop()
        {
            if (_currentIteration >= _numberOfIterations)
            {
                return true;
            }

            Matrix mat = new Matrix(1, _function.NumberOfVariables());
            for(int i = 0; i < _function.NumberOfVariables(); i++) {
                mat[0,i] = _previousBest.GetValueOfVariable(i);
            }

            if (_function.call(mat) < 1e-6)
            {
                return true;
            }

            return false;
        }

        private List<IUnit> CreatePopulation()
        {
            List<IUnit> units = new List<IUnit>(_populationSize);

            for (int i = 0; i < _populationSize; i++)
            {
                if (_unitType == UnitType.Binary)
                {
                    IUnit unit = new BinaryUnit(_lowerBound, _upperBound, _binaryPrecision, _point.GetNumberOfColumns(), _randomGenerator);
                    unit.CalculateFitness(_function);
                    units.Add(unit);
                }
                else
                {
                    IUnit unit = new DoubleUnit(_lowerBound, _upperBound, _point.GetNumberOfColumns(), _randomGenerator);
                    unit.CalculateFitness(_function);
                    units.Add(unit);
                }
            }

            units.Sort((u1, u2) => u2.GetFitness().CompareTo(u1.GetFitness()));
            return units;
        }

        private void CalculateFitness()
        {
            foreach (IUnit unit in _units)
            {
                unit.CalculateFitness(_function);
            }
        }

        private int GetWorstUnitIndex(List<int> selectedUnits)
        {
            double minFitness = double.MaxValue;
            int worstUnit = 0;

            foreach (int selectedUnit in selectedUnits)
            {
                if (_units[selectedUnit].GetFitness() < minFitness)
                {
                    minFitness = _units[selectedUnit].GetFitness();
                    worstUnit = selectedUnit;
                }
            }

            return worstUnit;
        }

        private IUnit GetBestUnit()
        {
            double maxFitness = double.MinValue;
            IUnit bestUnit = null;

            for (int i = 0; i < _units.Count; i++)
            {
                if (_units[i].GetFitness() > maxFitness)
                {
                    maxFitness = _units[i].GetFitness();
                    bestUnit = _units[i];
                }
            }

            return bestUnit;
        }

        private ICrossover GetCrossOver()
        {
            if (_unitType == UnitType.Binary)
            {
                return new BinaryUniformCrossover(_randomGenerator, _crossOverProbability);
            }
            else if (_unitType == UnitType.Double)
            {
                return new DoubleUniformCrossover(_randomGenerator, _crossOverProbability);
            }

            throw new NotImplementedException();
        }

        private IMutation GetMutation()
        {
            if (_unitType == UnitType.Binary)
            {
                return new BinaryMutation(_randomGenerator, _mutationProbability);
            }
            else if (_unitType == UnitType.Double)
            {
                return new DoubleMutation(_randomGenerator, _mutationProbability, 4, 1.2);
            }

            throw new NotImplementedException();
        }

        public void SetMutationProbability(double mutationProbability)
        {
            this._mutationProbability = mutationProbability;
        }

        public void SetPopulationSize(int populationSize)
        {
            this._populationSize = populationSize;
        }

        public void SetTournamentSize(int tournamentSize)
        {
            this._tournamentSize = tournamentSize;
        }
    }
}
