﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Constraints
{
    public interface ExplicitConstraint
    {
        bool IsSatisfied(Matrix point);
        double GetUpperBound();
        double GetLowerBound();
    }
}
