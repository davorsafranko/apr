﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Constraints
{
    public class SecondImplicitConstraint : ImplicitConstraint
    {
        public bool IsSatisfied(Matrix variables)
        {
            return variables[0, 0] <= 2;
        }

        public double GetValue(Matrix variables)
        {
            return 2 - variables[0, 0];
        }
    }
}
