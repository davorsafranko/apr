﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Constraints
{
    public class IntervalConstraint : ExplicitConstraint
    {
        private int lowerBound;
        private int higherBound;

        public IntervalConstraint(int lowerBound, int higherBound)
        {
            this.lowerBound = lowerBound;
            this.higherBound = higherBound;
        }

        public bool IsSatisfied(Matrix point)
        {
            bool retVal = true;

            for (int i = 0; i < point.GetNumberOfColumns(); i++)
            {
                if (point[0, i] > higherBound || point[0, i] < lowerBound)
                {
                    retVal = false;
                    break;
                }
            }

            return retVal;
        }

        public double GetUpperBound()
        {
            return higherBound;
        }

        public double GetLowerBound()
        {
            return lowerBound;
        }
    }
}
