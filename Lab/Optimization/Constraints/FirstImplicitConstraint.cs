﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Constraints
{
    public class FirstImplicitConstraint : ImplicitConstraint
    {
        public bool IsSatisfied(Matrix variables)
        {
            return variables[0, 1] >= variables[0, 0];
        }

        public double GetValue(Matrix variables)
        {
            return variables[0, 1] - variables[0, 0];
        }
    }
}
