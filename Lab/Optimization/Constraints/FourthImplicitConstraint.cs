﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Constraints
{
    public class FourthImplicitConstraint : ImplicitConstraint
    {
        public bool IsSatisfied(Matrix variables)
        {
            return (3 + 1.5*variables[0, 0] - variables[0, 1]) >= 0;
        }

        public double GetValue(Matrix variables)
        {
            return 3 + 1.5*variables[0, 0] - variables[0, 1];
        }
    }
}
