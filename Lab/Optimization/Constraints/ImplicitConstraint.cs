﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Constraints
{
    public interface ImplicitConstraint
    {
        bool IsSatisfied(Matrix variables);
        double GetValue(Matrix variables);
    }
}
