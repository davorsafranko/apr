﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.Constraints
{
    public class FirstExplicitConstraint : EqualityConstraint
    {
        public double GetValue(Matrix point)
        {
            return point[0, 1] - 1;
        }
    }
}
