﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.NumericIntegration
{
    public interface ISolver
    {
        Matrix Solve(Matrix A, Matrix start, double T, double Tmax);
    }
}
