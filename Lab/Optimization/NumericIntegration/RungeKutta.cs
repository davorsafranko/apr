﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.Optimization.NumericIntegration
{
    public class RungeKutta : ISolver
    {
        public Matrix Solve(Matrix A, Matrix start, double T, double Tmax)
        {
            Matrix xk = new Matrix(start);
            double t = 0;

            List<double> ts = new List<double>();
            ts.Add(t);
            List<double> xks1 = new List<double>();
            List<double> xks2 = new List<double>();
            xks1.Add(xk[0, 0]);
            xks2.Add(xk[0, 0]);

            while (t < Tmax)
            {
                Matrix m1 = A * xk;
                Matrix m2 = A * (xk + m1 * T * 0.5);
                Matrix m3 = A * (xk + m2 * T * 0.5);
                Matrix m4 = A * (xk + m3 * T);
                
                xk = xk + (m1 + m2 * 2 + m3 * 2 + m4) * T * (1/6.0);
                t += T;

                ts.Add(t);
                xks1.Add(xk[0, 0]);
                xks2.Add(xk[1, 0]);
            }

            string[] lines = new string[3];

            string line = "";
            for(int i = 0; i < ts.Count; i++)
            {
                line += ts[i] + ";";
            }

            line = line.Substring(0, line.Length - 1);
            lines[0] = line;

            line = "";
            for (int i = 0; i < xks1.Count; i++)
            {
                line += xks1[i] + ";";
            }

            line = line.Substring(0, line.Length - 1);
            lines[1] = line;

            line = "";
            for (int i = 0; i < xks2.Count; i++)
            {
                line += xks2[i] + ";";
            }

            line = line.Substring(0, line.Length - 1);
            lines[2] = line;

            System.IO.File.WriteAllLines("data.txt", lines);

            return xk;
        }
    }
}
