﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lab.Optimization;
using Lab.Optimization.Constraints;
using Lab.Optimization.Functions;

namespace Lab
{
    public class OptimizationAlgorithms
    {
        private readonly double _K = 0.5*(Math.Sqrt(5) - 1);
        private readonly double _h = 0.1;
        private double _epsilon;
        private double _startPoint;
        private AbstractFunction _function;
        private readonly double _sigma = 0.5;
        private int numberOfIterations = 0;

        public OptimizationAlgorithms()
        {
            string[] lines = System.IO.File.ReadAllLines("parameters.txt");
            Double.TryParse(lines[0], out _epsilon);
            string[] values = lines[1].Split(' ');

            if (values.Length == 1)
            {
                Double.TryParse(values[0], out _startPoint);
            }
        }

        public double GoldenRatioAlgorithm(double a, double b, double precision)
        {
            if (double.IsNaN(b))
            {
                //if given point, not interval
                var interval = FindUnimodalInterval(_h, a);
                a = interval.Item1;
                b = interval.Item2;
            }

            double c = b - _K*(b - a);
            double d = a + _K*(b - a);
            double fc = this._function.call(c);
            double fd = this._function.call(d);

            while ((b - a) > precision)
            {
                if (fc < fd)
                {
                    b = d;
                    d = c;
                    c = b - _K*(b - a);
                    fd = fc;
                    fc = this._function.call(c);
                }
                else
                {
                    a = c;
                    c = d;
                    d = a + _K*(b - a);
                    fc = fd;
                    fd = this._function.call(d);
                }
            }
            return (a + b)/2;
        }

        public Tuple<double, double> FindUnimodalInterval(double h, double point)
        {
            double left = point - h;
            double right = point + h;
            double m = point;
            double fl, fm, fr;
            uint step = 1;

            fm = this._function.call(point);
            fl = this._function.call(left);
            fr = this._function.call(right);

            if (fm < fr && fm < fl)
            {
                return new Tuple<double, double>(left, right);
            }
            else if (fm > fr)
                do
                {
                    left = m;
                    m = right;
                    fm = fr;
                    right = point + h * (step *= 2);
                    fr = this._function.call(right);
                } while (fm > fr);
            else
                do
                {
                    right = m;
                    m = left;
                    fm = fl;
                    left = point - h * (step *= 2);
                    fl = this._function.call(left);
                } while (fm > fl);

            return new Tuple<double, double>(left, right);
        }

        public Matrix CoordinateDescent(Matrix point, Matrix precisions)
        {
            this._function.ResetNumberOfCalls();
            Matrix x = new Matrix(point);
            do
            {
                Matrix xs = new Matrix(x);
                Matrix e = createNullVector(xs.GetNumberOfColumns());

                _function.SetVectors(xs, e);
                for (int i = 0; i < point.GetNumberOfColumns(); i++)
                {
                    e[0, i] = 1;

                    double lambda = GoldenRatioAlgorithm(x[0,i], double.NaN, precisions[0, i]);
                    x = x + lambda*e;

                    e[0, i] = 0;
                }

                bool stop = true;

                for (int i = 0; i < point.GetNumberOfColumns(); i++)
                {
                    if (Math.Abs(x[0, i] - xs[0, i]) > precisions[0, i])
                    {
                        stop = false;
                        break;
                    }
                }

                if (stop)
                {
                    break;
                }

            } while (true);

            return x;
        }

        public Matrix NelderMeadSimplex(Matrix point, double delta = 1,double alpha = 1, double beta = 0.5, double gamma = 2)
        {
            this._function.ResetNumberOfCalls();
            List<Matrix> simplex = new List<Matrix>();
            simplex[0] = new Matrix(point);

            for (int i = 0; i < point.GetNumberOfColumns(); i++)
            {
                simplex[i+1] = new Matrix(point);
                simplex[i+1][0, i] += delta;
            }

            Matrix xc;
            int n = simplex.Count - 1;
            do
            {
                SortSimplex(simplex);

                xc = createNullVector(simplex.Count - 1);
                for (int i = 0; i < simplex.Count - 1; i++)
                {
                    xc += simplex[i];
                }
                xc = xc * (1.0/ (n));

                Matrix xr = (1 + alpha)*xc - alpha*simplex[n];
                var val = this._function.call(xr);

                double val0 = _function.call(simplex[0]);
                double valNminus1 = _function.call(simplex[n - 1]);
                double valN = _function.call(simplex[n]);

                if ( (val >= val0) && (val < valNminus1))
                {
                    simplex[n] = new Matrix(xr);
                    
                }else if (val <val0)
                {
                    Matrix xe = (1 - gamma)*xc + gamma*xr;
                    if (this._function.call(xe) < val)
                    {
                        simplex[n] = new Matrix(xe);
                    }
                    else
                    {
                        simplex[n] = new Matrix(xr);
                    }
                }
                else
                {
                    Matrix xk = (1 - beta) * xc + beta * simplex[n];
                    if (this._function.call(xk) <= valN)
                    {
                        simplex[n] = new Matrix(xk);
                    }
                    else
                    {
                        for (int i = 1; i < simplex.Count; i++)
                        {
                            simplex[i] = (1 - _sigma)*simplex[0] + _sigma*simplex[i];
                        }
                    }
                }
            } while (!NelderMeadShouldStop(simplex,xc));

            return xc;
        }

        private bool NelderMeadShouldStop(List<Matrix> simplex, Matrix xc, double epsilon = 1e-6)
        {
            double fxc = this._function.call(xc);
            double sum = 0;

            for (int j = 0; j < simplex.Count; j++)
            {
                double val = this._function.call(simplex[j]);
                sum += Math.Pow(val - fxc, 2);
            }

            double num = Math.Sqrt((1.0/simplex.Count) *sum);

            _function.SubtractNumberOfCalls(4);

            if (num <= epsilon)
            {
                return true;
            }
            return false;
        }

        private void SortSimplex(List<Matrix> simplex)
        {
            for (int j = 0; j < simplex.Count - 1; j++)
            {
                int iMin = j;
                for (int i = j + 1; i < simplex.Count; i++)
                {
                    if (_function.call(simplex[i]) < _function.call(simplex[iMin]))
                    {
                        iMin = i;
                    }
                }

                if (iMin != j)
                {
                    var temp = simplex[iMin];
                    simplex[iMin] = simplex[j];
                    simplex[j] = temp;
                }
            }
        }
        public Matrix createNullVector(int nColumns)
        {
            Matrix mat = new Matrix(1, nColumns);

            for (int i = 0; i < nColumns; i++)
            {
                mat[0, i] = 0;
            }

            return mat;
        }

        public void SetFunction(AbstractFunction function)
        {
            this._function = function;
        }

        public Matrix HookeJeeves(Matrix point, Matrix dx, Matrix epsilons)
        {
            this._function.ResetNumberOfCalls();
            Matrix xp = new Matrix(point);
            Matrix xb = new Matrix(point);

            do
            {
                Matrix xn = Explore(xp, dx);
                if (_function.call(xn) < _function.call(xb))
                {
                    xp = 2*xn - xb;
                    xb = new Matrix(xn);
                }
                else
                {
                    for (int i = 0; i < dx.GetNumberOfColumns(); i++)
                    {
                        dx[0, i] *= 0.5;
                    }
                    xp = new Matrix(xb);
                }

            } while (!ShouldHookeJeevesStop(dx, epsilons));

            return new Matrix(xb);
        }

        public Matrix GradientDescent(Matrix point, bool useGoldenCut, double precision = 1e-6)
        {
            this.numberOfIterations = 0;

            this._function.ResetNumberOfCalls();
            Matrix currentPoint = new Matrix(point);
            double minValue = Double.MaxValue;
            double iterations = 0;
            while (true)
            {
                this.numberOfIterations++;
                var gradient = this._function.GetGradientInPoint(currentPoint);

                if (EuclideanNorm(gradient) < 1e-6)
                {
                    break;
                }

                if (iterations >= 100)
                {
                    Console.WriteLine("Gradient descent diverged.");
                    break;
                }

                double lambda = -1;

                if (useGoldenCut)
                {
                    this._function.SetVectors(currentPoint, gradient);
                    lambda = GoldenRatioAlgorithm(0, double.NaN, precision);
                }

                currentPoint = currentPoint + lambda * gradient;
                double val = this._function.call(currentPoint);
                if (val < minValue)
                {
                    minValue = val;
                    iterations = 0;
                }
                else
                {
                    iterations++;
                }
            }

            return currentPoint;
        }

        public Matrix NewtonRaphson(Matrix point, bool useGoldenCut, double precision = 1e-6)
        {
            this.numberOfIterations = 0;

            this._function.ResetNumberOfCalls();
            Matrix currentPoint = new Matrix(point);
            double minValue = Double.MaxValue;
            double iterations = 0;
            while (true)
            {
                this.numberOfIterations++;

                Matrix hesseMatrix = this._function.GetHesseMatrix(currentPoint);
                Matrix gradient = this._function.GetGradientInPoint(currentPoint);

                hesseMatrix.LU(gradient, true);

                hesseMatrix.SubstituteForward(gradient);
                hesseMatrix.SubstituteBackward(gradient);

                var deltaX = new Matrix(gradient);

                if (EuclideanNorm(deltaX) < 1e-6)
                {
                    break;
                }

                if (iterations >= 100)
                {
                    Console.WriteLine("Newton Raphson diverged.");
                    break;
                }

                double lambda = -1e-3;

                if (useGoldenCut)
                {
                    this._function.SetVectors(currentPoint, deltaX);
                    lambda = GoldenRatioAlgorithm(0, double.NaN, precision);
                }

                currentPoint = currentPoint + lambda * deltaX;
                double val = this._function.call(currentPoint);
                if (val < minValue)
                {
                    minValue = val;
                    iterations = 0;
                }
                else
                {
                    iterations++;
                }
            }

            return currentPoint;
        }

        public Matrix Box(Matrix point, ExplicitConstraint explicitConstraint, List<ImplicitConstraint> implicitConstraints, double alpha = 1.3, double precision = 1e-6)
        {
            this.numberOfIterations = 0;

            Random rand = new Random();
            Matrix xc = new Matrix(point);

            if (!explicitConstraint.IsSatisfied(xc))
            {
                throw new ArgumentException("Not all explicit constraints satisfied.");
            }

            if (implicitConstraints.Any(constraint => !constraint.IsSatisfied(xc)))
            {
                throw new ArgumentException("Not all implicit constraints satisfied.");
            }

            List<Matrix> simplex = new List<Matrix>();

            for (int i = 0; i < 2*xc.GetNumberOfColumns(); i++)
            {
                simplex.Add(new Matrix(point));
            }

            for (int i = 0; i < 2*xc.GetNumberOfColumns(); i++)
            {
                Matrix pt = new Matrix(1,xc.GetNumberOfColumns());

                for (int j = 0; j < xc.GetNumberOfColumns(); j++)
                {
                    double r = rand.NextDouble();
                    pt[0, j] = explicitConstraint.GetLowerBound() + r * (explicitConstraint.GetUpperBound() - explicitConstraint.GetLowerBound());
                }

                CheckImplicitConstraints(implicitConstraints, pt, xc);

                simplex[i] = pt;

                xc = CalculateCentroid(simplex);
            }

            double iterations = 0;
            double minValue = Double.MaxValue;
            while (true)
            {
                this.numberOfIterations++;

                if (iterations >= 100)
                {
                    Console.WriteLine("Box diverged.");
                    break;
                }

                SortSimplex(simplex);
                int worst = simplex.Count - 1;
                int secondWorst = simplex.Count - 2;

                xc = CalculateCentroid(simplex, false);
                Matrix xr = (1 + alpha)*xc - alpha*simplex[worst];

                for (int i = 0; i < xr.GetNumberOfColumns(); i++)
                {
                    if (xr[0,i] < explicitConstraint.GetLowerBound())
                    {
                        xr[0,i] = explicitConstraint.GetLowerBound();
                    }else if (xr[0, i] > explicitConstraint.GetUpperBound())
                    {
                        xr[0, i] = explicitConstraint.GetUpperBound();
                    }
                }

                CheckImplicitConstraints(implicitConstraints, xr, xc);

                if (this._function.call(xr) > this._function.call(simplex[secondWorst]))
                {
                    MoveToCenter(xr, xc);
                }

                simplex[worst] = xr;

                double val = this._function.call(xc);
                if (val < minValue)
                {
                    minValue = val;
                    iterations = 0;
                }
                else
                {
                    iterations++;
                }

                bool exit = true;
                for (int i = 0; i < xr.GetNumberOfColumns(); i++)
                {
                    if (Math.Abs(xr[0, i] - xc[0, i]) > precision)
                    {
                        exit = false;
                    }
                }

                if (exit)
                {
                    break;
                }
            }

            return xc;
        }

        private void CheckImplicitConstraints(List<ImplicitConstraint> constraints, Matrix pt, Matrix xc)
        {
            while (true)
            {
                bool sat = constraints.All(constraint => constraint.IsSatisfied(pt));

                if (sat)
                {
                    break;
                }
                MoveToCenter(pt, xc);
            }
        }

        private Matrix CalculateCentroid(List<Matrix> simplex, bool useWorst = true)
        {
            Matrix xc = new Matrix(1,simplex[0].GetNumberOfColumns());
            xc[0, 0] = 0;
            xc[0, 1] = 0;

            int length = useWorst ? simplex.Count : simplex.Count - 1;

            for (int i = 0; i < length; i++)
            {
                xc = xc + simplex[i];
            }

            return xc * (1.0 / (length));
        }

        public Matrix Transform(Matrix point, List<ImplicitConstraint> constraints, EqualityConstraint equalityConstraint,double t = 1, double precision = 1e-6)
        {
            Matrix previous = createNullVector(point.GetNumberOfColumns());
            Matrix current = createNullVector(point.GetNumberOfColumns());

            if (constraints.Any(constraint => !constraint.IsSatisfied(point)))
            {
                point = FindInternalPoint(point, constraints);
            }

            previous = point;

            OptimizationAlgorithms oab = new OptimizationAlgorithms();
            while (true)
            {
                oab.SetFunction(Factory.GetUFunction(constraints, equalityConstraint, this._function, t));

                current = oab.HookeJeeves(previous, new Matrix("Dx.txt"), new Matrix("ThirdEpsilons.txt"));
                numberOfIterations += oab.GetNumberOfIterations();
                bool exit = true;
                for (int i = 0; i < current.GetNumberOfColumns(); i++)
                {
                    if (Math.Abs(current[0, i] - previous[0, i]) > precision)
                    {
                        exit = false;
                    }
                }

                if (exit)
                {
                    break;
                }

                t = t*10;
                previous = current;
            }

            return current;
        }

        private Matrix FindInternalPoint(Matrix startingPoint, List<ImplicitConstraint> implicitConstraints)
        {
            OptimizationAlgorithms b = new OptimizationAlgorithms();
            b.SetFunction(Factory.GetFunction(implicitConstraints));

            Matrix newSP = b.HookeJeeves(startingPoint, new Matrix("Dx.txt"), new Matrix("ThirdEpsilons.txt"));
            return newSP;
        }

        private void MoveToCenter(Matrix point, Matrix xc)
        {
            point[0, 0] = 0.5 * (point[0, 0] + xc[0, 0]);
            point[0, 1] = 0.5 * (point[0, 1] + xc[0, 1]);
        }

        public int GetNumberOfIterations()
        {
            return this.numberOfIterations;
        }

        private double EuclideanNorm(Matrix vector)
        {
            return Math.Sqrt(Math.Pow(vector[0, 0], 2) + Math.Pow(vector[0, 1], 2));
        }

        private bool ShouldHookeJeevesStop(Matrix dx, Matrix epsilons)
        {
            for (int i = 0; i < dx.GetNumberOfColumns(); i++)
            {
                if (dx[0, i] > epsilons[0,i])
                {
                    return false;
                }
            }
            return true;
        }

        private Matrix Explore(Matrix xp, Matrix dx)
        {
            Matrix x = new Matrix(xp);

            for (int i = 0; i < xp.GetNumberOfColumns(); i++)
            {
                double p = this._function.call(x);
                x[0, i] += dx[0, i];
                double n = this._function.call(x);

                if (n > p)
                {
                    x[0, i] -= 2*dx[0, i];
                    n = this._function.call(x);
                    if (n > p)
                    {
                        x[0, i] += dx[0, i];
                    }
                }
            }

            return x;
        }

        public static AbstractFunction GetFunctionByName(string name)
        {
            if (name == "Rosenbrock")
            {
                return new RosenbrockFunction();
            }else if (name == "Second")
            {
                return new SecondFuction();
            }else if (name == "Third")
            {
                return new F3();
            }else if (name == "Jakobovic")
            {
                return new JakobovicFunction();
            }

            throw new NotImplementedException();
        }

        public int GetNumberOfFunctionCalls()
        {
            return _function.GetNumberOfCalls();
        }

        public double GetMinValue(Matrix x)
        {
            return _function.call(x);
        }

        public int GetNumberOfGradientCalculations()
        {
            return 0;
        }

        public int GetNumberOfHesseMatrixCalculations()
        {
            return _function.GetHesseCalls();
        }
    }
}
