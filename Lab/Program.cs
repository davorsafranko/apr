﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Lab.Optimization;
using Lab.Optimization.Constraints;
using Lab.Optimization.EvolutionaryAlgorithms;
using Lab.Optimization.EvolutionaryAlgorithms.Crossover;
using Lab.Optimization.EvolutionaryAlgorithms.Mutation;
using Lab.Optimization.EvolutionaryAlgorithms.Units;
using Lab.Optimization.Functions;

namespace Lab
{
    class Program
    {
        static void Main(string[] args)
        {
            #region lab1
            //1. zadatak
            //Matrix mat = new Matrix("1.txt");
            //for (int i = 0; i < mat.GetNumberOfRows(); i++)
            //{
            //    for (int j = 0; j < mat.GetNumberOfColumns(); j++)
            //    {
            //        double elem = mat[i, j];
            //        elem = elem / 1.33333;
            //        elem = elem * 1.33333;

            //        Console.WriteLine(elem == mat[i, j]);
            //    }
            //}

            //2.zadatak
            //Matrix a2 = new Matrix("LUP2.txt");
            //Matrix b2 = new Matrix("LUP2b.txt");

            //a2.Inverse().PrintToScreen();

            //a2.LU();
            //a2.PrintToScreen();

            //a2.SubstituteForward(b2);
            //a2.SubstituteBackward(b2);

            //b2.PrintToScreen();

            //3.zadatak
            //Matrix a3 = new Matrix("3a.txt");
            //Matrix b3 = new Matrix("3b.txt");

            ////a3.LU();
            //a3.LU(b3, true);
            //a3.PrintToScreen();

            //a3.SubstituteForward(b3);
            //a3.SubstituteBackward(b3);

            //b3.PrintToScreen();

            //4.zadatak
            //Matrix a4 = new Matrix("4a.txt");
            //Matrix b4 = new Matrix("4b.txt");

            //a4.LU();
            ////a4.LU(b4,true);
            //a4.PrintToScreen();

            //a4.SubstituteForward(b4);
            //a4.SubstituteBackward(b4);

            //b4.PrintToScreen();
            //5.zadatak
            //Matrix a5 = new Matrix("5a.txt");
            //Matrix b5 = new Matrix("5b.txt");

            //a5.LU(b5,true);
            //a5.PrintToScreen();

            //a5.SubstituteForward(b5);
            //a5.SubstituteBackward(b5);

            //b5.PrintToScreen();

            //6.zadatak
            //Matrix a6 = new Matrix("6a.txt");
            //Matrix b6 = new Matrix("6b.txt");

            //a6.LU();
            //a6.PrintToScreen();

            //a6.SubstituteForward(b6);
            //a6.SubstituteBackward(b6);

            //b6.PrintToScreen();

            #endregion

            #region lab2

            //1. zadatak

            //OptimizationAlgorithms oa = new OptimizationAlgorithms();
            //oa.SetFunction(new ThirdFunction());

            //List<double> points = new List<double>() { 0, 10, 20, 50, 100, 500, 1000, 10000 };

            //foreach (double point in points)
            //{
            //    Matrix p = oa.createNullVector(1);
            //    p[0, 0] = point;

            //    Console.WriteLine("Starting point: " + p);

            //    Matrix minCD = oa.CoordinateDescent(p, new Matrix("Fun3_1Epsilons.txt"));
            //    Console.WriteLine("Coordinate Descent Minimum: " + minCD + ", Number of calls:" + oa.GetNumberOfFunctionCalls());
            //    Matrix minNM = oa.NelderMeadSimplex(p);
            //    Console.WriteLine("Nelder Mead Simplex Minimum: " + minNM + ", Number of calls:" + oa.GetNumberOfFunctionCalls());
            //    Matrix minHJ = oa.HookeJeeves(p, new Matrix("Dx.txt"),
            //        new Matrix("ThirdEpsilons.txt"));
            //    Console.WriteLine("Hooke Jeeves Minimum: " + minHJ + ", Number of calls:" + oa.GetNumberOfFunctionCalls());

            //    Console.WriteLine();
            //}
            //Console.Read();

            //2. zadatak
            //List<string> functions = new List<string>() { "Rosenbrock", "Second", "Third", "Jakobovic" };
            //OptimizationAlgorithms oa = new OptimizationAlgorithms();

            //Console.ReadLine();
            //foreach (string functionName in functions)
            //{
            //    Console.WriteLine(functionName + " function:");
            //    oa.SetFunction(OptimizationAlgorithms.GetFunctionByName(functionName));

            //    if (functionName != "Jakobovic")
            //    {
            //        Matrix minCD = oa.CoordinateDescent(new Matrix(functionName + "Point.txt"), new Matrix(functionName + "Epsilons.txt"));
            //        Console.WriteLine("Coordinate Descent Minimum: " + minCD + ", Number of calls:" + oa.GetNumberOfFunctionCalls());
            //        Console.WriteLine("fmin() = " + oa.GetMinValue(minCD));
            //    }

            //    Matrix minNM = oa.NelderMeadSimplex(new Matrix(functionName + "Point.txt"));
            //    Console.WriteLine("Nelder Mead Simplex Minimum: " + minNM + ", Number of calls:" + oa.GetNumberOfFunctionCalls());
            //    Console.WriteLine("fmin() = " + oa.GetMinValue(minNM));

            //    Matrix minHJ = oa.HookeJeeves(new Matrix(functionName + "Point.txt"), new Matrix(functionName == "Third" ? functionName + "Dx.txt" : "Dx.txt"), new Matrix(functionName + "Epsilons.txt"));
            //    Console.WriteLine("Hooke Jeeves Minimum: " + minHJ + ", Number of calls:" + oa.GetNumberOfFunctionCalls());
            //    Console.WriteLine("fmin() = " + oa.GetMinValue(minHJ));

            //    Console.WriteLine();
            //}
            //Console.Read();

            //3. zadatak
            //OptimizationAlgorithms oa = new OptimizationAlgorithms();
            //oa.SetFunction(new JakobovicFunction());
            //Matrix minNM = oa.NelderMeadSimplex(new Matrix("Jakobovic_4Point.txt"));
            //Console.WriteLine("Nelder Mead Simplex Minimum: " + minNM + ", Number of calls:" + oa.GetNumberOfFunctionCalls());
            //Matrix minHJ = oa.HookeJeeves(new Matrix("Jakobovic_4Point.txt"), new Matrix("Dx.txt"), new Matrix("JakobovicEpsilons.txt"));
            //Console.WriteLine("Hooke Jeeves Minimum: " + minHJ + ", Number of calls:" + oa.GetNumberOfFunctionCalls());

            //Console.Read();

            //4. zadatak    
            //OptimizationAlgorithms oa = new OptimizationAlgorithms();
            //oa.SetFunction( new RosenbrockFunction());

            //Console.WriteLine("Starting point : 0.5 0.5");
            //for (int i = 1; i <= 20; i++)
            //{
            //    if (i != 5 && i != 12)
            //    {
            //        Matrix minNM = oa.NelderMeadSimplex(new Matrix("Rosenbrock_4Point.txt"), i);
            //        Console.WriteLine("Step: " + i);
            //        Console.WriteLine("Nelder Mead Simplex Minimum: " + minNM + ", Number of calls:" + oa.GetNumberOfFunctionCalls());
            //        Console.WriteLine("fmin() = " + oa.GetMinValue(minNM));
            //        Console.WriteLine();
            //    }
            //}

            //Console.WriteLine("Starting point : 20 20");
            //for (int i = 1; i <= 20; i++)
            //{
            //    Matrix minNM = oa.NelderMeadSimplex(new Matrix("Rosenbrock_41Point.txt"), i);
            //    Console.WriteLine("Step: " + i);
            //    Console.WriteLine("Nelder Mead Simplex Minimum: " + minNM + ", Number of calls:" + oa.GetNumberOfFunctionCalls());
            //    Console.WriteLine("fmin() = " + oa.GetMinValue(minNM));
            //    Console.WriteLine();
            //}

            //Console.Read();

            //5. zadatak

            //OptimizationAlgorithms oa = new OptimizationAlgorithms();
            //oa.SetFunction(new SchafferFunction());

            //Random rnd = new Random();
            //for (int i = 0; i < 1000; i++)
            //{
            //    double next = rnd.NextDouble()*100 - 50;
            //    Matrix x = oa.createNullVector(2);
            //    x[0, 0] = next;
            //    x[0, 1] = next;

            //    Matrix hj = oa.HookeJeeves(x, new Matrix("Dx.txt"), new Matrix("SchafferEpsilons.txt"));
            //    double fmin = oa.GetMinValue(hj);

            //    if (fmin < 1e-4)
            //    {
            //        Console.WriteLine("fmin() = "+  fmin + ", point = " + next + " " + next);
            //    }
            //}
            //Console.Read();

            #endregion

            #region lab3
            //OptimizationAlgorithms oa = new OptimizationAlgorithms();

            //1. zadatak
            //oa.SetFunction(new ThirdFunction());
            //Matrix point = new Matrix(1, 2);
            //point[0, 0] = -1.9;
            //point[0, 1] = 2;
            //Console.WriteLine("Calculating optimal step  value");
            //Matrix min = oa.GradientDescent(point, true);
            //Console.WriteLine($"Min : ({min[0, 0]}, {min[0, 1]}), number of iterations : {oa.GetNumberOfIterations()}");

            //Console.WriteLine("Without calculating optimal step  value");
            //Matrix minN = oa.GradientDescent(point, false);
            //Console.WriteLine($"Min : ({minN[0, 0]}, {minN[0, 1]}), number of iterations : {oa.GetNumberOfIterations()}");

            //2.zadatak
            //Matrix point = new Matrix(1, 2);

            //Console.WriteLine("Rosenbrock");
            //oa.SetFunction(new RosenbrockFunction());
            //point[0, 0] = -1.9;
            //point[0, 1] = 2;
            //Matrix min = oa.NewtonRaphson(point, true);
            //Console.WriteLine("Newton Raphson");
            //Console.WriteLine($"Min : ({min[0, 0]}, {min[0, 1]}), number of iterations : {oa.GetNumberOfIterations()}");
            //min = oa.GradientDescent(point, true);
            //Console.WriteLine("Gradient Descent");
            //Console.WriteLine($"Min : ({min[0, 0]}, {min[0, 1]}), number of iterations : {oa.GetNumberOfIterations()}");

            //Console.WriteLine();

            //Console.WriteLine("Second Function");
            //oa.SetFunction(new SecondFuction());
            //point[0, 0] = 0.1;
            //point[0, 1] = 0.3;
            //Matrix minS = oa.NewtonRaphson(point, true);
            //Console.WriteLine("Newton Raphson");
            //Console.WriteLine($"Min : ({minS[0, 0]}, {minS[0, 1]}), number of iterations : {oa.GetNumberOfIterations()}");
            //min = oa.GradientDescent(point, true);
            //Console.WriteLine("Gradient Descent");
            //Console.WriteLine($"Min : ({min[0, 0]}, {min[0, 1]}), number of iterations : {oa.GetNumberOfIterations()}");

            //3.zadatak
            //Matrix point = new Matrix(1, 2);
            //Console.WriteLine("Rosenbrock");
            //oa.SetFunction(new RosenbrockFunction());
            //point[0, 0] = -1.9;
            //point[0, 1] = 2;

            //List<ImplicitConstraint> implicitConstraints = new List<ImplicitConstraint>();
            //implicitConstraints.Add(new FirstImplicitConstraint());
            //implicitConstraints.Add(new SecondImplicitConstraint());

            //Matrix minS = oa.Box(point, new IntervalConstraint(-100, 100), implicitConstraints);
            //Console.WriteLine($"Min : ({minS[0, 0]}, {minS[0, 1]}), number of iterations : {oa.GetNumberOfIterations()}");

            //Console.WriteLine("Second Function");
            //oa.SetFunction(new SecondFuction());
            //point[0, 0] = 0.1;
            //point[0, 1] = 0.3;

            //minS = oa.Box(point, new IntervalConstraint(-100, 100), implicitConstraints);
            //Console.WriteLine($"Min : ({minS[0, 0]}, {minS[0, 1]}), number of iterations : {oa.GetNumberOfIterations()}");

            //4. zadatak
            //Matrix point = new Matrix(1, 2);
            //oa.SetFunction(new RosenbrockFunction());
            //point[0, 0] = -1.9;
            //point[0, 1] = 2;

            //List<ImplicitConstraint> implicitConstraints = new List<ImplicitConstraint>();
            //implicitConstraints.Add(new FirstImplicitConstraint());
            //implicitConstraints.Add(new SecondImplicitConstraint());

            //Matrix min = oa.Transform(point, implicitConstraints, null);
            //Console.WriteLine($"Min : ({min[0, 0]}, {min[0, 1]})");

            //oa.SetFunction(new SecondFuction());
            //point[0, 0] = 0.1;
            //point[0, 1] = 0.3;

            //min = oa.Transform(point, implicitConstraints, null);
            //Console.WriteLine($"Min : ({min[0, 0]}, {min[0, 1]})");

            //5. zadatak
            //Matrix point = new Matrix(1, 2);
            //oa.SetFunction(new FourthFunction());
            //point[0, 0] = 5;
            //point[0, 1] = 5;

            //List<ImplicitConstraint> implicitConstraints = new List<ImplicitConstraint>();
            //implicitConstraints.Add(new FourthImplicitConstraint());
            //implicitConstraints.Add(new ThirdImplicitConstraint());

            //EqualityConstraint eqC = new FirstExplicitConstraint();

            //Matrix min = oa.Transform(point, implicitConstraints, eqC);
            //Console.WriteLine($"Min : ({min[0, 0]}, {min[0, 1]})");

            #endregion

            #region lab4

            //zad1
            //Console.WriteLine("Rosenbrock");
            //Console.WriteLine();

            //Matrix point = new Matrix("RosenbrockPoint.txt");
            //AbstractFunction function = new RosenbrockFunction();
            //EvolutionaryAlgorithm alg1 = new EvolutionaryAlgorithm("parameters.txt", function, point);
            ////alg1.GenerationalAlgorithm();

            //Console.WriteLine();
            //Console.WriteLine("Third function");

            //point = new Matrix("ThirdFunctionPoint.txt");
            //function = new F3();
            //EvolutionaryAlgorithm alg2 = new EvolutionaryAlgorithm("parameters.txt", function, point);
            ////alg2.GenerationalAlgorithm();

            //Console.WriteLine();
            //Console.WriteLine("Schaffer function");
            //Console.WriteLine();

            //point = new Matrix("SchafferFunctionPoint.txt");
            //function = new SchafferFunction();
            //EvolutionaryAlgorithm alg3 = new EvolutionaryAlgorithm("parameters.txt", function, point);
            ////alg3.GenerationalAlgorithm();

            //Console.WriteLine();
            //Console.WriteLine("Altered Schaffer function");
            //Console.WriteLine();

            //point = new Matrix("SchafferFunctionPoint.txt");
            //function = new AlteredSchafferFunction();
            //EvolutionaryAlgorithm alg4 = new EvolutionaryAlgorithm("parameters.txt", function, point);
            //alg4.GenerationalAlgorithm();

            //zad2
            //string[] dimensions = new string[] { "SchafferFunctionPoint2.txt", "SchafferFunctionPoint3.txt", "SchafferFunctionPoint6.txt", "SchafferFunctionPoint10.txt" };

            //Console.WriteLine("Schaffer function");

            //foreach (string fileName in dimensions)
            //{
            //    Matrix point = new Matrix(fileName);

            //    SchafferFunction function = new SchafferFunction(point.GetNumberOfColumns());

            //    Console.WriteLine();
            //    Console.WriteLine("Dimensions : " + point.GetNumberOfColumns());
            //    Console.WriteLine();

            //    EvolutionaryAlgorithm alg = new EvolutionaryAlgorithm("parameters.txt", function, point);
            //    Console.WriteLine(alg.GenerationalAlgorithm().GetFitness());
            //}

            //Console.WriteLine();
            //Console.WriteLine("Altered Schaffer function");
            //Console.WriteLine();

            //foreach (string fileName in dimensions)
            //{
            //    Matrix point = new Matrix(fileName);

            //    AlteredSchafferFunction func = new AlteredSchafferFunction(point.GetNumberOfColumns());

            //    Console.WriteLine();
            //    Console.WriteLine("Dimensions : " + point.GetNumberOfColumns());
            //    Console.WriteLine();

            //    EvolutionaryAlgorithm alg = new EvolutionaryAlgorithm("parameters.txt", func, point);
            //    Console.WriteLine(alg.GenerationalAlgorithm().GetFitness());
            //}

            //zad3
            //Console.WriteLine("Schaffer function");
            //Console.WriteLine();

            //Matrix point = new Matrix("SchafferFunctionPoint3.txt");
            //AbstractFunction function = new SchafferFunction(point.GetNumberOfColumns());
            //List<IUnit> binary3 = GetBestUnits(function, point, "parameters1.txt");
            //Console.WriteLine("Binary3 : " + GetScore(binary3));
            //Console.WriteLine("Median : " + GetMedian(binary3));

            //point = new Matrix("SchafferFunctionPoint3.txt");
            //function = new SchafferFunction(point.GetNumberOfColumns());
            //List<IUnit> double3 = GetBestUnits(function, point, "parameters2.txt");
            //Console.WriteLine("Double3 : " + GetScore(double3));
            //Console.WriteLine("Median : " + GetMedian(double3));

            //point = new Matrix("SchafferFunctionPoint6.txt");
            //function = new SchafferFunction(point.GetNumberOfColumns());
            //List<IUnit> binary6 = GetBestUnits(function, point, "parameters1.txt");
            //Console.WriteLine("Binary6 : " + GetScore(binary6));
            //Console.WriteLine("Median : " + GetMedian(binary6));


            //point = new Matrix("SchafferFunctionPoint6.txt");
            //function = new SchafferFunction(point.GetNumberOfColumns());
            //List<IUnit> double6 = GetBestUnits(function, point, "parameters2.txt");
            //Console.WriteLine("Double6 : " + GetScore(double6));
            //Console.WriteLine("Median : " + GetMedian(double6));

            //Console.WriteLine();
            //Console.WriteLine("Altered Schaffer function");
            //Console.WriteLine();

            //point = new Matrix("SchafferFunctionPoint3.txt");
            //function = new AlteredSchafferFunction(point.GetNumberOfColumns());
            //binary3 = GetBestUnits(function, point, "parameters1.txt");
            //Console.WriteLine("Binary3 : " + GetScore(binary3));
            //Console.WriteLine("Median : " + GetMedian(binary3));

            //point = new Matrix("SchafferFunctionPoint6.txt");
            //function = new AlteredSchafferFunction(point.GetNumberOfColumns());
            //binary6 = GetBestUnits(function, point, "parameters1.txt");
            //Console.WriteLine("Binary6 : " + GetScore(binary6));
            //Console.WriteLine("Median : " + GetMedian(binary6));

            //point = new Matrix("SchafferFunctionPoint3.txt");
            //function = new AlteredSchafferFunction(point.GetNumberOfColumns());
            //double3 = GetBestUnits(function, point, "parameters2.txt");
            //Console.WriteLine("Double3 : " + GetScore(double3));
            //Console.WriteLine("Median : " + GetMedian(double3));

            //point = new Matrix("SchafferFunctionPoint6.txt");
            //function = new AlteredSchafferFunction(point.GetNumberOfColumns());
            //double6 = GetBestUnits(function, point, "parameters2.txt");
            //Console.WriteLine("Double6 : " + GetScore(double6));
            //Console.WriteLine("Median : " + GetMedian(double6));

            //zad4
            int[] populationSizes = new int[4] { 30, 50, 100, 200 };
            double[] mutationProbabilities = new double[4] { 0.1, 0.3, 0.6, 0.9 };

            Matrix point = new Matrix("SchafferFunctionPoint2.txt");
            AbstractFunction function = new SchafferFunction(point.GetNumberOfColumns());

            //foreach (int populationSize in populationSizes)
            //{
            //    foreach (double mutationProbability in mutationProbabilities)
            //    {
            //        List<IUnit> units = GetBestUnits(function, point, "parameters2.txt",3, populationSize, mutationProbability);
            //        Console.WriteLine("Population size : {0}, Mutation probability : {1}, Median : {2}", populationSize, mutationProbability, GetMedian(units));
            //    }
            //}
            string[] lines = new string[10];
            List<List<IUnit>> unitsByPopulationSize = new List<List<IUnit>>();
            foreach (int populationSize in populationSizes)
            {
                List<IUnit> units = GetBestUnits(function, point, "parameters2.txt", 3, populationSize, -1);
                unitsByPopulationSize.Add(units);
            }

            for(int i = 0; i < 10; i++)
            {
                lines[i] = "";
            }

            for(int i = 0; i < unitsByPopulationSize[0].Count; i++)
            {
                for(int j = 0; j < unitsByPopulationSize.Count; j++)
                {
                    lines[i] += unitsByPopulationSize[j][i].GetFitness() + ";";
                }
            }

            for (int i = 0; i < 10; i++)
            {
                lines[i] = lines[i].Substring(0, lines[i].Length - 1);
            }

            System.IO.File.WriteAllLines("boxplot.txt", lines);
            //zad5
            //int[] ks = new int[] {3,4,5};
            //AbstractFunction function = new SchafferFunction();
            //Matrix point = new Matrix("SchafferFunctionPoint.txt");

            //foreach (int k in ks)
            //{
            //    List<IUnit> units = GetBestUnits(function, point, k);
            //    Console.WriteLine("Tournament size : {0}, Median : {1}", k, GetMedian(units));
            //}
            #endregion

            #region Lab5
            //zad1

            //Matrix njihalo = new Matrix("MatematickoNjihalo.txt");
            //Matrix uvjeti = new Matrix("MatematickoPocetni.txt");
            //Matrix parametri = new Matrix("Parametri.txt");

            //Optimization.NumericIntegration.ISolver solver = new Optimization.NumericIntegration.TrapezeSolver();
            //solver.Solve(njihalo, uvjeti, parametri[0,0], parametri[0,1]).PrintToScreen();

            //zad2
            //Matrix njihalo = new Matrix("FizickoNjihalo.txt");
            //Matrix uvjeti = new Matrix("FizickoPocetni.txt");
            //Matrix parametri = new Matrix("Parametri.txt");

            //Optimization.NumericIntegration.ISolver solver = new Optimization.NumericIntegration.TrapezeSolver();
            //solver.Solve(njihalo, uvjeti, parametri[0, 0], parametri[0, 1]).PrintToScreen();
            #endregion
        }

        public static List<IUnit> GetBestUnits(AbstractFunction function, Matrix point, string parametersFileName, int tournamentSize = 3, int populationSize = -1, double mutationProbability = -1)
        {
            List<IUnit> bestUnits = new List<IUnit>(10);

            for (int i = 0; i < 10; i++)
            {
                EvolutionaryAlgorithm alg = new EvolutionaryAlgorithm(parametersFileName, function, point, false);
                if (populationSize != -1)
                {
                    if(mutationProbability != -1) {
                        alg.SetMutationProbability(mutationProbability);
                    }
                    alg.SetPopulationSize(populationSize);
                }
                bestUnits.Add(alg.GenerationalAlgorithm());
            }


            return bestUnits;
        }

        public static int GetScore(List<IUnit> units)
        {
            int score = 0;

            foreach (IUnit unit in units)
            {
                if ((1.0/unit.GetFitness()) < 1e-6)
                {
                    score++;
                }
            }

            return score;
        }

        public static double GetMedian(List<IUnit> units)
        {
            List<double> fitness = new List<double>(units.Count);

            foreach (var unit in units)
            {
                fitness.Add(unit.GetFitness());
            }

            fitness.Sort();

            return (fitness[units.Count/2] + fitness[(units.Count + 2)/2]) / 2;
        }
    }
}
